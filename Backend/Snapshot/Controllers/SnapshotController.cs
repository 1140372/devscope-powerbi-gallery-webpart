﻿using NReco.PhantomJS;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Routing;

namespace Snapshot.Controllers
{
    public class SnapshotController : ApiController
    {

        [Route("api/Snapshot/pbi/{biToken}")]
        // GET: api/Snapshot/pbi/{biToken}
        public HttpResponseMessage Get(String biToken)
        {
            HttpRequestHeaders headers = this.Request.Headers;
            string token = string.Empty;
            if (headers.Contains("token")) {
                token = headers.GetValues("token").First();
            }
            var url = "https://app.powerbi.com/view?r=" + biToken;
            this.goPhantom(url, token, biToken);
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(getImageMemStream(biToken).ToArray());
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
            return result;
        }

        private void goPhantom(string url, string token, string imageName)
        {
            String scriptPath = HostingEnvironment.MapPath("~/Scripts/snapshot.js");
            var phantomJS = new PhantomJS();
            string[] args = { url, token, "800", "600", imageName};
            phantomJS.Run(scriptPath, args);
            phantomJS.OutputReceived += (sender, e) => {
                Console.WriteLine("PhantomJS output: {0}", e.Data);
            };
        }

        private MemoryStream getImageMemStream(string imageName)
        {
            String filePath = HostingEnvironment.MapPath("~/bin/"+ imageName+".png");
            FileStream fileStream = new FileStream(filePath, FileMode.Open);
            Image image = Image.FromStream(fileStream);
            MemoryStream memoryStream = new MemoryStream();
            image.Save(memoryStream, ImageFormat.Jpeg);
            return memoryStream;
        }
    }
}
