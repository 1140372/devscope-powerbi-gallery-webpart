var system = require('system');
var args = system.args;
var token, url, width, height, html;

if (args.length === 1) {
    console.log('Try to pass some arguments when invoking this script!');
    phantom.exit();
} else {
    url = args[1];
    token = args[2];
    width = args[3];
    height = args[4];
    name = args[5]
}

function onPageReady() {
    setTimeout(function () {
        phantom.exit();
    }, 5000);
    page.render(name+".png");
}

var m = {
    action: 'loadReport',
    accessToken: token
};
var message = JSON.stringify(m);



var page = require('webpage').create();
page.viewportSize = {
    width: width,
    height: height
};

page.open(url, function (status) {
    function checkReadyState() {
        setTimeout(function () {
            var readyState = page.evaluate(function () {
                return document.readyState;
            });
            if ("complete" === readyState) {
                page.evaluate(function (message) {
                    window.postMessage(message, '*');
                }, message);
                setTimeout(function () {
                    onPageReady();
                }, 5000);

            } else {
                checkReadyState();
            }
        });
    }
    checkReadyState();
});