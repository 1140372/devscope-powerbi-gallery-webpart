export interface IReport{
  title: string;
  URL: string;
}

export interface IPowerBiGalleryWebPartProps {
  reportTitle: string;
  reportURL: string;
  title: string;
  selectedList: string;
  token: string;
}
