import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField,
  IPropertyPaneDropdownOption,
  PropertyPaneButton,
  PropertyPaneButtonType,
  PropertyPaneDropdown
} from '@microsoft/sp-webpart-base';
import * as strings from 'powerBiGalleryStrings';
import PPaneCustomDropDownField from './PPaneCustomDropDown';
import PowerBiGallery from './components/Gallery/PowerBiGallery';
import { SPHttpClient, HttpClientResponse } from '@microsoft/sp-http';
import { IPowerBiGalleryProps } from './components/Gallery/IPowerBiGalleryProps';
import { IPowerBiGalleryWebPartProps } from './IPowerBiGalleryWebPartProps';
import pnp from 'sp-pnp-js';
import MyWeb from './Web';

export default class PowerBiGalleryWebPart extends BaseClientSideWebPart<IPowerBiGalleryWebPartProps> {

  private reportLists: IPropertyPaneDropdownOption[];
  private propertyPaneLayout: any;
  private web: MyWeb;

  public onInit(): Promise<void> {
    return super.onInit().then(_ => {
      pnp.setup({
        spfxContext: this.context,
        headers: {
          'Accept': 'application/json;odata=nometadata'
        }
      });
      this.reportLists = [];
      this._getPowerBI();
      this._setPropertyPaneBaseLayout();
      this.web = pnp.sp.web.as(MyWeb);
      this._getListsFromSP();
      this.web.ensureTileList('ReportList', 'Lista default de reports').then(_ => {
        console.log('Default list created');
      });
    });
  }

  public render(): void {
    const element: React.ReactElement<IPowerBiGalleryProps> = React.createElement(
      PowerBiGallery,
      {
        title: this.properties.title,
        httpClient: this.context.spHttpClient,
        Url: "https://app.powerbi.com/reportEmbed?reportId=b44cbe28-854e-42f4-9392-9ba982a4468f",
        token: this.properties.token
      }
    );

    ReactDom.render(element, this.domElement);
  }

 
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

 private _getPowerBI(): void {

      const body: string = JSON.stringify({
        'resource': 'https://analysis.windows.net/powerbi/api'
      });

      this.context.spHttpClient.post(this.context.pageContext.web.absoluteUrl + `/_api/SP.OAuth.Token/Acquire`, SPHttpClient.configurations.v1, {
        headers: {
          'Accept': 'application/json;odata=nometadata',
          'Content-type': 'application/json;odata=verbose',
          'odata-version': ''
        },
        body: body
      })
        .then((response: HttpClientResponse) => {
          debugger;
          return response.json();
        })
        .then(responseObject => {
          console.log(responseObject.access_token)
          this.properties.token = responseObject.access_token;
          this.render();
        });
  }
  
  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          groups: [
            this.propertyPaneLayout
          ]
        }
      ]
    };
  }

  private _changeLayoutToReportEditor() {
    this.propertyPaneLayout = ({
      groupName: 'Report Editor',
      groupFields: [
        PropertyPaneTextField('reportTitle', {
          label: 'Title:'
        }),
        PropertyPaneTextField('reportURL', {
          label: 'Report URL:'
        }),
        PropertyPaneButton('', {
          buttonType: PropertyPaneButtonType.Primary,
          text: 'Create',
          onClick: this._addReportToList.bind(this)
        }),
        PropertyPaneButton('', {
          buttonType: PropertyPaneButtonType.Normal,
          text: 'Cancel',
          onClick: this._setPropertyPaneBaseLayout.bind(this)
        })
      ]
    });
    this.context.propertyPane.refresh();
  }

  private _setPropertyPaneBaseLayout() {
    this.propertyPaneLayout = ({
      groupName: 'Base',
      groupFields: [
        PropertyPaneTextField('Title', {
          label: 'Title'
        }),
        // new PPaneCustomDropDownField('',{
        //   label: 'Select a Report list:',
        //   dropDownOptions: this.reportLists,
        //   onSelectAction: this._addReportToList.bind(this),
        //   selectedItem: ''
        // }),
        PropertyPaneDropdown('selectedList', {
          label: 'Select a list of reports:',
          options: this.reportLists,
          selectedKey: this.properties.selectedList
        }),
        PropertyPaneButton('', {
          buttonType: PropertyPaneButtonType.Primary,
          text: 'Add new report',
          onClick: this._changeLayoutToReportEditor.bind(this)
        })
      ]
    });
    this.context.propertyPane.refresh();
  }

  private _getListsFromSP() {
    return this.web.lists.get().then((response: any) => {
      for (var i = 0; i < response.length; i++) {
        this.reportLists.push({
          key: response[i].Id,
          text: response[i].Title
        });
      }
    });
  }

  private _addReportToList() {
    debugger;
    return this.web.lists.getById(this.properties.selectedList).items.add({
      Title: this.properties.reportTitle,
      URL: {
        Url: this.properties.reportURL,
        Description: this.properties.reportTitle
      }
    }).then((response: any) => {
      this._getListsFromSP();
      this.properties.reportTitle = undefined;
      this.properties.reportURL = undefined;
      this._setPropertyPaneBaseLayout();
    });
  }
}
