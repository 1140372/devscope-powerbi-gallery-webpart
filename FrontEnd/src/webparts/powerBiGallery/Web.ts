import {
    ListEnsureResult,
    Web,
    FieldAddResult,
} from 'sp-pnp-js';


export default class MyWeb extends Web {

    public ensureTileList(tileListName: string, description: string) {
        return this.lists.ensure(tileListName, description, 100).then((ler: ListEnsureResult) => {
            let queries = this._getQueries();
            if (ler.created) {
                ler.list.defaultView.get().then((view: any) => {
                    (function loop(i) {
                        ler.list.fields.createFieldAsXml(queries[i]).then((field: FieldAddResult) => {
                            ler.list.getView(view.Id).fields.add(field.data.InternalName)
                                .then(_ => i > queries.length || loop(i + 1));
                        });
                    })(0);
                });
            }
        });
    }

    private _getQueries() {
        return [
            `<Field Name="PBI_URL" DisplayName="URL" Type="URL" Required="TRUE" />`
        ];
    }
}

