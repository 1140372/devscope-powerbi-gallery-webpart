import { SPHttpClient } from '@microsoft/sp-http';

export interface IPowerBiGalleryProps {
  title: string;
  httpClient: SPHttpClient;
  Url: string;
  token: string;
}
