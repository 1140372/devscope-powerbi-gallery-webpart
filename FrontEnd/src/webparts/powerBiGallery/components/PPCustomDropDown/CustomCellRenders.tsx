import * as React from 'react';
import { IDropdownOption } from 'office-ui-fabric-react';

export default class cellRender {
    public static renderColorOption(props?: IDropdownOption): JSX.Element {
        return (
            <div className="metro">
                <li className="unstyled">
                    <span className={"square10 inline-block bg-" + props.text + " on-left"}></span>
                    {props.text}
                </li>
            </div>);
    }

    public static renderIconOption(props?: IDropdownOption): JSX.Element{
        return(
            <div>
                <span className={"square10 inline-block ms-Icon ms-Icon--" + props.text + " on-left"}></span>
                {" "+props.text}
            </div>            
        );
    }

    public static renderTextOption(props?: IDropdownOption): JSX.Element{
        return(
            <div>
                <span>{props.text}</span>
            </div>
        );
    }
}