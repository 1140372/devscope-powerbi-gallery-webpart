import * as React from 'react';
import { SPHttpClient, HttpClientResponse } from '@microsoft/sp-http';
import { IReportProps } from './ReportProps';
import {Button} from 'office-ui-fabric-react';

export interface reportState{
    loadedUrl: string;
}
export default class Report extends React.Component<IReportProps, reportState>{

    public constructor(props){
        super(props);
        this.setState({loadedUrl:this.props.Url})
    }

    public render() {
        return (
            <div>
                <iframe 
                id={'powerBIFrame'} 
                src={this.props.Url} 
                frameBorder="0" 
                onLoad={this.postActionLoadReport.bind(this)}
                height={600}
                width={800}
                ></iframe>
            </div>
        );
    }
    
  private postActionLoadReport(): void {
    var iframe: any = document.getElementById('powerBIFrame');
    var m = {
      action: "loadReport",
      accessToken: this.props.accessToken
    };
    var message = JSON.stringify(m);

    iframe.contentWindow.postMessage(message, "*");;
  }
}