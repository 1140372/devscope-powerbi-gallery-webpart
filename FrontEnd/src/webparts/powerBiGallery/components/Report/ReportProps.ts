import { SPHttpClient } from '@microsoft/sp-http';

export interface IReportProps{
    Url: string;
    HttpClient: SPHttpClient;
    accessToken: string;
}