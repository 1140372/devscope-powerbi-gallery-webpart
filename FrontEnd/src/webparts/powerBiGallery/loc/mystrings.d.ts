declare interface IPowerBiGalleryStrings {
  //PropertyPane 
  WebPartTitle: string;
  CancelBTN: string;
  //PropertyPane List Editor
  ListTitle: string;
  ListDescription: string;
  CreateListBTN: string;

  //PropertyPane Item Editor
  ItemTitle: string;
  ItemURL: string;
  CreateItemBTN: string;

}

declare module 'powerBiGalleryStrings' {
  const strings: IPowerBiGalleryStrings;
  export = strings;
}
